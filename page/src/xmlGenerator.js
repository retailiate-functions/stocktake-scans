export const xmlGenerator = (totalData, kunde, auftragsnr, fileName) => {


  let data = totalData || null;
  if (data == null || !data.length) {
    return null;
  }
  let columnDelimiter = ",";
  let lineDelimiter = "\n";

  function create_xml(data) {
    console.log("XXXX")
    let result = "";

    var i;
    for (i = 0; i< data.length; i++) {
      var item = data[i]
      var amount = item.amount
      var ean = item.ean

      var item_string = `		<twarenkorbpos>
 		   <cName/>
 		   <cArtNr/>
 		   <cBarcode>${ean}</cBarcode>
  		  <cEinheit/>
  		  <fPreisEinzelNetto>0.0</fPreisEinzelNetto>
  		  <fMwSt>19.00</fMwSt>
  		  <fAnzahl>${amount}.00</fAnzahl>
  		  <cPosTyp>standard</cPosTyp><!-- Pflichtfeld, siehe XSD für Werte -->
  		  <fRabatt>0.00</fRabatt>
		</twarenkorbpos>
`
      result += item_string
    }
    return result
  }
  let items = create_xml(totalData);

  var result = `<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
	XML Beispieldatei für den Import über Verkauf>Auftragsimport oder über die tXMLBestellimport Tabelle.

	Wenn kein Kommentar angegeben ist, ist das Feld Pflicht zum angeben!

	Felder die gefüllt sein müssen haben einen extra Kommentar!

	Optionale Felder dürfen NICHT angegeben werden ohne gefüllt zu sein !!!
-->
<tBestellungen>
	<tBestellung><!--cRechnungsNr, kFirma und kBenutzer sind Optional-->
		<cSprache>ger</cSprache><!-- Pflichtfeld, siehe XSD für Werte -->
		<cWaehrung>EUR</cWaehrung><!-- Pflichtfeld, siehe XSD für Werte -->
		<fGuthaben>0.00</fGuthaben><!-- Optional -->
		<fGesamtsumme>0.00</fGesamtsumme><!-- Optional -->
		<cBestellNr>${auftragsnr}</cBestellNr>
		<cExterneBestellNr/>
		<cVersandartName>Selbstabholer</cVersandartName>
		<cVersandInfo/>
		<dVersandDatum/>
		<cTracking/>
		<dLieferDatum/>
		<cKommentar/>
		<cBemerkung/>
        <dErstellt>20.02.2020</dErstellt>
		<cZahlungsartName>Bar</cZahlungsartName>
		<dBezahltDatum/>
		<fBezahlt>0.00</fBezahlt><!--Optional-->
${items}
		<tkunde> <!-- Hier kann noch kKunden angegeben werden -->
			<cKundenNr>${kunde}</cKundenNr>
			<cAnrede/>
			<cTitel/>
			<cVorname/>
			<cNachname/>
			<cFirma/>
			<cStrasse>1</cStrasse>
			<cAdressZusatz/>
			<cPLZ/>
			<cOrt/>
			<cBundesland/>
            <cLand>Deutschland</cLand>
			<cTel/>
			<cMobil/>
			<cFax/>
			<cMail/>
			<cUSTID/>
			<cWWW/>
            <cNewsletter>N</cNewsletter>
            <dGeburtstag>01.01.2020</dGeburtstag>
            <fRabatt>0</fRabatt>
            <cHerkunft>WaWi</cHerkunft>
		</tkunde>
		<tlieferadresse>
			<cAnrede/>
			<cVorname/>
			<cNachname/>
			<cTitel/>
			<cFirma/>
			<cStrasse>1</cStrasse>
			<cAdressZusatz/>
			<cPLZ/>
			<cOrt/>
			<cBundesland/>
            <cLand>Deutschland</cLand>
			<cTel/>
			<cMobil/>
			<cFax/>
			<cMail/>
		</tlieferadresse>
		<tzahlungsinfo>
			<cBankName/>
			<cBLZ/>
			<cKontoNr/>
			<cKartenNr/>
			<dGueltigkeit/>
			<cCVV/>
			<cKartenTyp/>
			<cInhaber/>
			<cIBAN/>
			<cBIC/>
		</tzahlungsinfo>
	</tBestellung>
</tBestellungen>
`

  if (result == null) return;

  var blob = new Blob([result]);
  if (navigator.msSaveBlob) {
    // IE 10+
    navigator.msSaveBlob(blob, exportedFilenmae);
  } else if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
    var hiddenElement = window.document.createElement("a");
    hiddenElement.href = "data:text/csv;charset=utf-8," + encodeURI(result);
    hiddenElement.target = "_blank";
    hiddenElement.download = fileName;
    hiddenElement.click();
  } else {
    let link = document.createElement("a");
    if (link.download !== undefined) {
      // Browsers that support HTML5 download attribute
      var url = URL.createObjectURL(blob);
      link.setAttribute("href", url);
      link.setAttribute("download", fileName);
      link.style.visibility = "hidden";
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
};

