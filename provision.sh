#!/usr/bin/env bash

cd /tmp/provision

if [[ -f "/tmp/provision/function/frecklets/build.frecklet" ]]; then
  /root/.local/share/freckles/bin/frecklecute -c callback=default::full -r /tmp/provision/function/frecklets /tmp/provision/function/frecklets/build.frecklet
else
  echo "No additional provisioning."
fi

