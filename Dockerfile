FROM node:10.15.2 as NODE

WORKDIR /usr/src/app

COPY page             page

RUN cd page && npm install && npm run build

FROM registry.gitlab.com/retailiate/retailiate-core:dev

# Set up of-watchdog for HTTP mode
ENV fprocess="python3 index.py"
ENV cgi_headers="true"
ENV mode="http"
ENV upstream_url="http://127.0.0.1:5000"
ENV write_debug="false"

HEALTHCHECK --interval=5s CMD [ -e /tmp/.lock ] || exit 1

RUN curl -sSL https://github.com/openfaas-incubator/of-watchdog/releases/download/0.7.6/of-watchdog > /usr/bin/fwatchdog && \
           chmod +x /usr/bin/fwatchdog && \
           adduser --disabled-password --gecos '' app

WORKDIR /home/app/

COPY --from=NODE /usr/src/app/page/public/ page/


COPY index.py           .
COPY python           python

RUN touch ./python/__init__.py

RUN chown -R app:app ./

USER app
WORKDIR /home/app/

RUN pip3 install --user -r python/requirements.txt

ENV PATH=$PATH:/home/app/.local/bin

CMD ["fwatchdog"]
