# Copyright (c) Markus Binsteiner, 2019. All rights reserved.
# Licensed under the MIT license. See LICENSE file in the project root for full license information.
from starlette.exceptions import HTTPException
from starlette.middleware.cors import CORSMiddleware
from starlette.staticfiles import StaticFiles

from retailiate_core.core import Retailiate, Context
from python import entrypoint
import logging
import uvicorn
from starlette.applications import Starlette
from starlette.routing import Route, Mount

from retailiate_core.utils.rest_api import format_response

async def main_route(request):

    try:
        context = Context()

        result = await entrypoint.handle(request, context)

        return format_response(result)
    except (HTTPException) as he:
        raise he
    except (Exception) as e:
        return HTTPException(status_code=500, detail=str(e))


static = StaticFiles(directory='page', html=True)

app = Starlette(debug=False, routes=[
    Route('/', main_route, methods=["POST", "PUT", "PATCH", "DELETE"]),
    Route('/{path}', main_route, methods=["POST", "PUT", "PATCH", "DELETE"]),
    Mount('/', static, name="static"),
    # Route('/{path}', get_route, methods=["GET"]),
])
# app.add_middleware(CORSMiddleware, allow_origins=["*"], allow_methods=["*"])

if __name__ == '__main__':

    uvicorn.run(app, host="0.0.0.0", port=5000, log_level="info")

