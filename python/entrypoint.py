import logging

from aiographql.client.transaction import GraphQLRequest
from starlette.responses import Response
from typing import Dict

from retailiate_core.core import Retailiate, Context
from starlette.requests import Request

retailiate = Retailiate()


async def delete_last_item(device_id: int):

    logging.info("DELETE LAST ITEM")

    query = """mutation DeleteLastItemFromDevice($device_id:Int!) {
  delete_stocktake_scans(where: {last_scan: {device_id: {_eq: $device_id}}}) {
    affected_rows
    returning {
    id
      ean_sum_for_device {
        amount
      }
      ean
      device_id
      amount
    }
  }
}"""
    req = GraphQLRequest(
        query=query,
        variables={"device_id": device_id},
        operationName="DeleteLastItemFromDevice",
        validate=False,
    )

    result = await retailiate.hasura_client.post(req)
    logging.info(result.response)
    logging.info(result.response.data)
    logging.info(result.response.__dict__)

    res_data = result.response.data["delete_stocktake_scans"]["returning"][0]
    ean = res_data["ean"]
    amount = res_data["amount"]
    sum = res_data["ean_sum_for_device"]["amount"]
    result_string = f"Geloescht: {ean} ({sum}/-{str(amount)})"

    return result_string


async def post_item(variables):

    logging.info("POST ITEM")

    query = """mutation InsertScannedItem($input:[stocktake_scans_insert_input!]!){
      insert_stocktake_scans(objects:$input) {
        returning {
          id
          amount
          ean_sum_for_device {
             amount
          }
        }
      }
    }
    """

    req = GraphQLRequest(
        query=query, variables=variables, operationName="InsertScannedItem"
    )

    logging.info("BEFORE REQUEST: {}".format(variables))
    result = await retailiate.hasura_client.post(req)
    logging.info("AFTER REQUEST: {}".format(result))
    logging.info(result.response)
    logging.info(result.response.data)
    logging.info(result.response.__dict__)
    return result.response.data


async def get_amount(ean: str):

    query = """query AmountOfItem($ean: String) {
  stocktake_scans_aggregate(where: {ean: {_eq: $ean}}) {
    aggregate {
      sum {
        amount
      }
    }
  }
}"""
    logging.info("GET AMOUNT")
    req = GraphQLRequest(
        query=query, variables={"ean": ean}, operationName="AmountOfItem"
    )

    result = await retailiate.hasura_client.post(req)
    logging.info(result.response)
    logging.info(result.response.data)
    logging.info(result.response.__dict__)
    return result.response.data["stocktake_scans_aggregate"]["aggregate"]["sum"][
        "amount"
    ]


async def get_ean_amount_for_device(ean: str, device_id: int) -> int:

    query = """query SumEanForDevice($device_id: Int!, $ean: String!) {
  stocktake_sum_ean_per_device(where: {device_id: {_eq: $device_id}, ean: {_eq: $ean}}) {
    amount
    device_id
    ean
  }
}"""

    req = GraphQLRequest(
        query=query,
        variables={"ean": ean, "device_id": device_id},
        operationName="SumLastEanForDevice",
    )

    result = await retailiate.hasura_client.post(req)
    logging.info(result.response)
    logging.info(result.response.data)
    logging.info(result.response.__dict__)
    return result.response.data["stocktake_sum_ean_per_device"][0]["amount"]


async def get_ean_sum_last_scan(device_id) -> int:

    query = """query SumEanLastScanEan($device_id: Int!) {
  stocktake_sum_ean(where: {ean_sum_for_last_device_ean: {device_id: {_eq: $device_id}}}) {
    amount
    ean
  }
}"""

    req = GraphQLRequest(
        query=query,
        variables={"device_id": device_id},
        operationName="SumEanLastScanEan",
        validate=False,
    )

    result = await retailiate.hasura_client.post(req)
    logging.info(result.response)
    logging.info(result.response.data)
    logging.info(result.response.__dict__)
    return result.response.data["stocktake_sum_ean"][0]["amount"]


async def get_ean_sum_for_device_last_scan(device_id: int) -> int:

    query = """query SumLastEanForDevice($device_id: Int!) {
  stocktake_sum_ean_per_device(where: {device_sum_for_last_device_ean: {device_id: {_eq: $device_id}}}) {
    device_sum_for_last_device_ean {
      device_id
      ean
      last_id
    }
    amount
  }
}
    
"""

    req = GraphQLRequest(
        query=query,
        variables={"device_id": device_id},
        operationName="SumLastEanForDevice",
        validate=False,
    )

    result = await retailiate.hasura_client.post(req)
    logging.info(result.response)
    logging.info(result.response.data)
    logging.info(result.response.__dict__)
    return result.response.data["stocktake_sum_ean_per_device"][0]["amount"]


async def add_scanned_item(data: Dict):

    migration_result = await post_item({"input": data})  # Execute the query

    # id = migration_result["insert_stocktake_scans"]["returning"][0]["id"]
    last_added = migration_result["insert_stocktake_scans"]["returning"][0]["amount"]
    sum_ean = migration_result["insert_stocktake_scans"]["returning"][0][
        "ean_sum_for_device"
    ]["amount"]

    # logging.info("RESULT MIGRATION: {} / {} / {}".format(migration_result, id, last_added))
    # amount = await get_ean_amount_for_device(ean=data["ean"], device_id=data["device_id"])

    last_added_string = f"(+{str(last_added)})"

    result = f"Momentan gesamt: {sum_ean} {last_added_string}"
    return result


def construct_xml(msg, success=True):

    status = 1 if success else 0
    xml = """<?xml version="1.0" encoding="UTF-8"?>
    <xml>
        <message>
            <status>{}</status>
            <text>{}</text>
        </message>
    </xml>""".format(
        status, msg
    )

    return xml


async def handle_ctrl_instruction(data):

    success = True
    if data["ean"] == "-del-":
        logging.info(f"deleting last item from: {data['device_id']}")
        msg = await delete_last_item(data["device_id"])
    elif data["ean"] == "-inf-":
        logging.info(
            f"getting info for item '{data['ean']}' for device: {data['device_id']}"
        )
        amount = await get_ean_sum_for_device_last_scan(device_id=data["device_id"])
        msg = f"Gescannt (dieses Geraet): {amount}"
    elif data["ean"] == "-infa-":
        logging.info(f"getting info for item '{data['ean']}'")
        amount = await get_ean_sum_last_scan(device_id=data["device_id"])
        msg = f"Gescannt (gesamt): {amount}"
    else:
        msg = f"Unknow ctrl instruction: {data['ean']}"
        logging.error(msg)
        success = False

    return msg, success


VALID_USERS = [
    "TSchuhmacher",
    "SSchuhmacher",
    "TClauss",
    "SBesemer",
    "NBinder",
    "MHernandez",
    "MChurch",
    "BEulau",
    "MGriessmayr",
    "RMaurer",
    "LPoese",
    "KRau",
    "JSuess",
    "AViehauser",
    "LPaltaller",
    "SYurt",
    "MKoschorreck",
    "TLobenstein",
    "SEberl",
    "EStockhammer",
    "MBacher",
    "AOppenhaeuser",
    "TWimmer",
    "RWeber",
    "TEST"
]


async def handle(request: Request, context: Context):
    """handle a request to the function
    Args:
        req: request body
    """

    logging.info(request)
    try:
        data = await request.form()
        logging.info(data)
        clean = {}
        clean["user_id"] = int(data["userid"])
        clean["ean"] = data["tid"]
        clean["sid"] = int(data["sid"])
        clean["device_id"] = int(data["deviceid"])
        clean["udid"] = data["udid"]
        clean["capture_type"] = data["capture_type"]
        # obj.timestamp= data["device_timestamp"]
        clean["scan_time"] = data["scanned_at_utc"]
        clean["barcode_format"] = data.get("barcode_format", "unknown")
        clean["amount"] = data.get("answers[1686298]", 1)
        clean["warehouse_id"] = int(data.get("answers[1759908]", 1))

        user = data.get("answers[1818934]", None)
        if user is None:
            xml = construct_xml("Kein Benutzer angemeldet", success=False)
            return Response(xml, media_type="text/xml")

        if user not in VALID_USERS:
            xml = construct_xml(f"Ungueltiger Benutzer: {user}", success=False)
            return Response(xml, media_type="text/xml")

        clean["user"] = user

        logging.info(f"data: {clean}")

        success = True

        if clean["ean"].startswith("-") and clean["ean"].endswith("-"):
            logging.info(f"control instruction detected: {clean['ean']}")
            msg, success = await handle_ctrl_instruction(clean)
        else:
            ean = clean['ean']

            try:
                int(ean)
            except (ValueError):
                msg = f"Ungueltige ean: {ean}"
                success = False

            if success:

                if len(ean) != 13:
                    ean = ean.zfill(13)
                    clean['ean'] = ean

                logging.info(f"adding item: {clean['ean']} (device: {clean['device_id']})")
                msg = await add_scanned_item(clean)

        xml = construct_xml(msg, success=success)

    except (Exception) as e:
        logging.error(e, exc_info=1)
        xml = construct_xml(str(e), success=False)

    return Response(xml, media_type="text/xml")
